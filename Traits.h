// ###############################################################################################################
// Copyright 2018, Jakub Mrowinski, All rights reserved.
// ###############################################################################################################
#pragma once
#include <any>
#include <tuple>
#include <string>
#include <stdio.h>
#include "EnumAsConstexpr.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Test entry point
void RunTraitsTest();

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// System extension ID
enum class EID : int
{
	Triangle2D,
	Sphere3D,
	Number,
	Count
};

ENABLE_ENUM_AS_CONSTEXPR(EID, EID::Count);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// System core. Stores all info accessible at compile time.
template<EID id>
struct Traits
{
	using ParamsType = std::tuple<std::false_type>;
	static constexpr const char* Name = "";
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Helper macro to define traits in one line.
#define DEFINE_EXT(id, name, ...) \
template<> \
struct Traits<id> \
{ \
	using ParamsType = std::tuple<__VA_ARGS__>; \
	static constexpr const char* Name = name; \
};

using vec2 = std::array<float, 2>;
using vec3 = std::array<float, 3>;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Different arg types, different arg count.
DEFINE_EXT(EID::Triangle2D, "Triangle", vec2, vec2, vec2);
DEFINE_EXT(EID::Sphere3D, "Sphere", vec3, double);
DEFINE_EXT(EID::Number, "Number", unsigned int);