// ###############################################################################################################
// Copyright 2018, Jakub Mrowinski, All rights reserved.
// ###############################################################################################################
#pragma once

// Allows using runtime enum values as compile time params.
// Note: This could be implemented as a pair of function templates requiring a Count member at the end of an enum.
// That would, however, severely limit its application with legacy code.
// With this macro, users can explicitly specify the upper bound.
#define ENABLE_ENUM_AS_CONSTEXPR(enum_type, upper_bound) \
template<short L, short R, short MID = (L + R) / 2, bool VALID = ((L <= R) && (R <= static_cast<short>(upper_bound)))> \
struct enum_type##Constexpr_Internal \
{ \
    template<typename FunctorType, typename... Args> \
    inline static decltype(auto) UseIn(enum_type e, Args&&... args) \
    { \
        if (static_cast<short>(e) < MID) \
        { \
            return enum_type##Constexpr_Internal<L, MID - 1>::template UseIn<FunctorType>(e, std::forward<Args>(args)...); \
        } \
        else if (static_cast<short>(e) > MID) \
        { \
            return enum_type##Constexpr_Internal<MID + 1, R>::template UseIn<FunctorType>(e, std::forward<Args>(args)...); \
        } \
        else \
        { \
            return FunctorType::template Action<static_cast<enum_type>(MID)>(std::forward<Args>(args)...); \
        } \
    } \
    \
    \
    \
    template<typename PredicateType, typename... Args> \
    inline static enum_type FindIf(Args&&... args) \
    { \
        if (PredicateType::template Check<static_cast<enum_type>(L)>(std::forward<Args>(args)...)) \
        { \
            return static_cast<enum_type>(L); \
        } \
        else \
        { \
            return enum_type##Constexpr_Internal<L+1, R>::template FindIf<PredicateType>(std::forward<Args>(args)...); \
        } \
    } \
    \
}; \
\
template<short L, short R, short MID> \
struct enum_type##Constexpr_Internal<L, R, MID, false> \
{ \
    template<typename FunctorType, typename... Args> \
    inline static decltype(auto) UseIn(enum_type, Args&&... args) \
    { \
        return FunctorType::template Action<upper_bound>(std::forward<Args>(args)...); \
    } \
    \
    template<typename PredicateType, typename... Args> \
    inline static enum_type FindIf(Args&&...) \
    { \
        return upper_bound; \
    } \
}; \
\
using enum_type##Constexpr = enum_type##Constexpr_Internal<0, static_cast<short>(upper_bound)>;
