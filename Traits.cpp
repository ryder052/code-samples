// ###############################################################################################################
// Copyright 2018, Jakub Mrowinski, All rights reserved.
// ###############################################################################################################
#include "stdafx.h"
#include "Traits.h"
#include <fstream>
#include <array>
#include <vector>

// The following should be specialized / overloaded for any needed data type.
template<typename T>
bool Serialize(const T& object, std::ofstream& target)
{
	if constexpr(std::is_fundamental_v<T>)
	{
		target << object << '\n';
		return true;
	}
	else
	{
		return false;
	}
}

template<typename T>
bool Deserialize(T& object, std::ifstream& source)
{
	if constexpr(std::is_fundamental_v<T>)
	{
		source >> object;
		return true;
	}
	else
	{
		return false;
	}
}

template<typename T, size_t size>
bool Serialize(const std::array<T, size>& arr, std::ofstream& target)
{
	for (auto&& obj : arr)
		if (!Serialize(obj, target))
			return false;

	return true;
}

template<typename T, size_t size>
bool Deserialize(std::array<T, size>& arr, std::ifstream& source)
{
	for (auto&& obj : arr)
		if (!Deserialize(obj, source))
			return false;

	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// A couple of generic actions to use with EnumAsConstexpr
namespace IDActions
{
	struct CreateParams
	{
		template<EID id>
		static std::any Action()
		{
			return std::make_any<typename Traits<id>::ParamsType>();
		}
	};

	struct GetName
	{
		template<EID id>
		static const char* Action()
		{
			return Traits<id>::Name;
		}
	};

	struct SaveParams
	{
		template<EID id>
		static bool Action(std::any& params, std::ofstream& file)
		{
			return SaveParamsImpl(std::any_cast<typename Traits<id>::ParamsType&>(params), file);
		}

		template<typename Tuple, size_t tid = 0>
		static bool SaveParamsImpl(Tuple& tuple, std::ofstream& file)
		{
			// Save each tuple element.
			if constexpr(tid < std::tuple_size_v<Tuple>)
			{
				auto& value = std::get<tid>(tuple);
				if (!Serialize(value, file))
					return false;

				return SaveParamsImpl<Tuple, tid + 1>(tuple, file);
			}

			return true;
		}
	};

	struct LoadParams
	{
		template<EID id>
		static bool Action(std::any& params, std::ifstream& file)
		{
			params = IDActions::CreateParams::Action<id>();
			return LoadParamsImpl(std::any_cast<typename Traits<id>::ParamsType&>(params), file);
		}

		template<typename Tuple, size_t tid = 0>
		static bool LoadParamsImpl(Tuple& tuple, std::ifstream& file)
		{
			// Load each tuple element.
			if constexpr(tid < std::tuple_size_v<Tuple>)
			{
				auto& value = std::get<tid>(tuple);
				if (!Deserialize(value, file))
					return false;

				return LoadParamsImpl<Tuple, tid + 1>(tuple, file);
			}

			return true;
		}
	};

	template <typename T, typename Tuple>
	struct has_type;

	template <typename T, typename... Us>
	struct has_type<T, std::tuple<Us...>> : std::disjunction<std::is_same<T, Us>...> {};

	template<typename Type, size_t Idx = 0>
	struct GetParam
	{
		template<EID id>
		static Type& Action(std::any& params)
		{
			using TType = typename Traits<id>::ParamsType;
			auto& casted_params = std::any_cast<TType&>(params);

			// This must compile for everything we define in traits.
			if constexpr(has_type<Type, TType>::value && Idx < std::tuple_size_v<TType>)
			{
				if constexpr(Idx == 0)
				{
					// Get only by type.
					return std::get<Type>(casted_params);
				}
				else
				{
					// Get by both type and index. 
					// reinterpret_cast needed to compile with "wrong types", but these are always dealt with by if checks.
					return reinterpret_cast<Type&>(std::get<Idx>(casted_params));
				}
			}
			else
			{
				// We never get here, but this must always return something.
				static Type* dummy = nullptr;
				return *dummy;
			}
		}
	};

	template<typename Type, size_t Idx = 0>
	struct SetParam
	{
		template<EID id>
		static void Action(std::any& params, const Type& value)
		{
			using TType = typename Traits<id>::ParamsType;
			auto& casted_params = std::any_cast<TType&>(params);

			// This must compile for everything we define in traits.
			if constexpr(has_type<Type, TType>::value && Idx < std::tuple_size_v<TType>)
			{
				if constexpr(Idx == 0)
				{
					// Get only by type.
					std::get<Type>(casted_params) = value;
				}
				else
				{
					// Get by both type and index. 
					// reinterpret_cast needed to compile with "wrong types", but these are always dealt with by if checks.
					reinterpret_cast<Type&>(std::get<Idx>(casted_params)) = value;
				}
			}
		}
	};
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Generic system class, holds and works with any data.
struct GenericClass
{
	GenericClass(const std::string& file_name)
		: m_Path(file_name)
	{}

	bool Save()
	{
		std::ofstream file;
		file.open(m_Path);

		// Save action id
		Serialize(static_cast<int>(m_ID), file);

		if (!EIDConstexpr::UseIn<IDActions::SaveParams>(m_ID, m_Params, file))
			return false;

		file.close();
		return true;
	}

	bool Load()
	{
		std::ifstream file;
		file.open(m_Path);

		if (!file.good())
			return false;

		// Load action id
		int id_tmp;
		Deserialize(id_tmp, file);
		m_ID = static_cast<EID>(id_tmp);

		if (!EIDConstexpr::UseIn<IDActions::LoadParams>(m_ID, m_Params, file))
			return false;

		std::printf("Loaded a %s.\n", EIDConstexpr::UseIn<IDActions::GetName>(m_ID));

		file.close();
		return true;
	}

	std::any m_Params;
	std::string m_Path;
	EID m_ID = EID::Count;
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Test suite
void TestSphereChange()
{
	GenericClass obj("sphere.gcd");

	// Load and change or initialize
	if (!obj.Load())
	{
		obj.m_ID = EID::Sphere3D;
		obj.m_Params = EIDConstexpr::UseIn<IDActions::CreateParams>(obj.m_ID);
		std::printf("Created a new %s.\n", EIDConstexpr::UseIn<IDActions::GetName>(obj.m_ID));

		// Set initial values.
		EIDConstexpr::UseIn<IDActions::SetParam<vec3>>(obj.m_ID, obj.m_Params, vec3{ 7.5f, 1.1f, 4.6f });
		EIDConstexpr::UseIn<IDActions::SetParam<double>>(obj.m_ID, obj.m_Params, 10.0);

		// Retrieve values and print them.
		vec3 center = EIDConstexpr::UseIn<IDActions::GetParam<vec3>>(obj.m_ID, obj.m_Params);
		double radius = EIDConstexpr::UseIn<IDActions::GetParam<double, 1>>(obj.m_ID, obj.m_Params);
		std::printf("Center = [%.1f, %.1f, %.1f], radius %.1f\n", center[0], center[1], center[2], radius);
	}
	else
	{
		// Retrieve values and print them.
		vec3 center = EIDConstexpr::UseIn<IDActions::GetParam<vec3>>(obj.m_ID, obj.m_Params);
		double radius = EIDConstexpr::UseIn<IDActions::GetParam<double, 1>>(obj.m_ID, obj.m_Params);
		std::printf("Center = [%.1f, %.1f, %.1f], radius %.1f\n", center[0], center[1], center[2], radius);

		// Modify and set.
		center[0] += 100.0f;
		EIDConstexpr::UseIn<IDActions::SetParam<vec3>>(obj.m_ID, obj.m_Params, center);

		// Change in-place, since GetParam returns a ref.
		EIDConstexpr::UseIn<IDActions::GetParam<vec3>>(obj.m_ID, obj.m_Params)[1] += 100.0f;

		// One more way to edit. It seems weird to just edit x and y ;)
		float* z_ptr = &EIDConstexpr::UseIn<IDActions::GetParam<vec3>>(obj.m_ID, obj.m_Params)[2];
		*z_ptr += 100.0f;

		// Get again and print.
		center = EIDConstexpr::UseIn<IDActions::GetParam<vec3>>(obj.m_ID, obj.m_Params);
		std::printf("New center = [%.1f, %.1f, %.1f], radius %.1f\n", center[0], center[1], center[2], radius);
	}

	obj.Save();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Entry point
void RunTraitsTest()
{
	TestSphereChange();
}