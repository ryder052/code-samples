// ###############################################################################################################
// Copyright 2018, Jakub Mrowinski, All rights reserved.
// ###############################################################################################################
#include "stdafx.h"
#include "Event.h"

enum class Tests
{
	Add,
	AddMultiplied,
	AddRemotely,
	PrintString,
	Count
};

struct Test
{
	Test()
	{
		auto& mgr = EventManager<Tests>::GetInstance();

		mgr.DeclareEvent<Tests::Add, int>();
		mgr.DeclareEvent<Tests::AddRemotely, int>();
		mgr.DeclareEvent<Tests::AddMultiplied, double, double>();
		mgr.DeclareEvent<Tests::PrintString, std::string*>();

		c1 = mgr.AddEventListener<Tests::Add>(this, &Test::Add1, 1);
		c2 = mgr.AddEventListener<Tests::Add>(this, &Test::Add10, 10);

		c3 = mgr.AddEventListener<Tests::PrintString>(this, &Test::PrintString);

		remote_conn = mgr.AddEventListener<Tests::AddRemotely>(this, &Test::Add100ViaEvents, 100);

		// This should emit a warning due to the discarded Connection object. Who's gonna manage it now?
		mgr.AddEventListener<Tests::AddMultiplied>(this, &Test::AddMultiplied);
	}

	~Test()
	{
		c1.Disconnect();
		c2.Disconnect();
		c3.Disconnect();
	}

	void Add1(int n)
	{
		N += n;
	}

	void Add10(int n)
	{
		N += 10*n;
	}

	void AddMultiplied(double n, double m)
	{
		N += int(n * m);
	}

	void Add100ViaEvents(int n)
	{
		for (int i=0; i<10; ++i)
			EventManager<Tests>::GetInstance().TriggerEvent<Tests::Add>(n);
	}

	void PrintString(std::string* str)
	{
		std::printf("%s\n", str->c_str());
	}

	void Print()
	{
		std::printf("N: %d\n", N);
	}

	int N = 0;
	Connection c1, c2, c3;
	ScopedConnection remote_conn;
};

void RunEventsTest()
{
	auto& mgr = EventManager<Tests>::GetInstance();
	auto Error = []() { std::printf("Error!\n"); };

	{
		Test test;

		// Call all events bound to Add
		if (mgr.TriggerEvent<Tests::Add>(3))
			test.Print();
		else
			Error();

		// Call event from another event
		if (mgr.TriggerEvent<Tests::AddRemotely>(10))
			test.Print();
		else
			Error();

		// Different args and arg count
		if (mgr.TriggerEvent<Tests::AddMultiplied>(3000.0, 1.0 / 3.0))
			test.Print();
		else
			Error();

		// Print something.
		std::string test_str = "TestString";
		if (!mgr.TriggerEvent<Tests::PrintString>(&test_str))
			Error();

		// Disconnect. N should not change during this trigger.
		test.remote_conn.Disconnect();
		if (mgr.TriggerEvent<Tests::AddRemotely>(10))
			test.Print();
		else
			Error();

		// test object goes out of scope, all connections should be destroyed
	}

	// This should do nothing now.
	mgr.TriggerEvent<Tests::Add>(888);

	// Memory cleanup.
	mgr.DestroyAll();
}
